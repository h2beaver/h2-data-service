package h2beaver.tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.map.MultiValueMap;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.linearref.LengthIndexedLine;

import h2beaver.smm.model.CrossSection;
import h2beaver.smm.utils.CrossSectionUtils;
import h2beaver.sobek.lit.Common;
import h2beaver.sobek.lit.FileLoader;
import h2beaver.sobek.lit.Network;
import h2beaver.sobek.lit.NetworkNTW;
import h2beaver.sobek.lit.Profile;

public class LoadCrossSection implements Common {
    public static final String NETWORK_NTW = "NETWORK.NTW";
    public static final String NETWORK_CR = "NETWORK.CR";
    public static final String PROFILE_DEF = "PROFILE.DEF";
    public static final String PROFILE_DAT = "PROFILE.DAT";

    public static void showUsage(String errorMessage) {
        if (errorMessage != null && errorMessage.length() > 0) {
            System.err.println(errorMessage);
            System.err.println();
        }
        System.err.println(
                "USAGE: java -cp h2beaver-fat.jar h2beaver.tool.LoadCrossSection {project} {lit-path} {git-project-path} {new-lit-path}");
        System.err.println();
        System.err.println(
                "Example: java -cp h2beaver-fat.jar h2beaver.tool.LoadCrossSection yilan ./yilan_demo/YL_TC_DEMO/YL_TC_D.lit/27 ./git-repos/ ./new-lit");
        System.exit(-1);
    }

    public static File checkArguments(String[] args) throws IOException {
        if (args.length < 4) {
            showUsage("");
        }

        Stream.of(args[1], args[2]).forEach(path -> {
            File f = new File(path);
            if (!f.exists()) {
                showUsage(f.getName() + " doesn't exists");
            }
        });
        File f3 = new File(args[3]);
        if (!f3.exists()) {
            Files.createDirectories(f3.toPath());
        }
        return f3;
    }

    public static void main(String[] args) throws Exception {
        File outputPath = checkArguments(args);
        String region = args[0];

        String projectName = Common.getRepoName(region);

        Path outputCrossSectionProject = Paths.get(args[2], projectName, "src", "main", "resources");
        List<CrossSection> crossSections = CrossSectionUtils
                .loadCrossSectionUnderFolder(outputCrossSectionProject.toFile(), "X");

        FileLoader l = new FileLoader(Paths.get(args[1]));
        Network network = Network.load(l);
        MultiValueMap branchesToUpdate = new MultiValueMap();

        try (FileWriter network_cr = new FileWriter(new File(outputPath, NETWORK_CR));
                FileWriter profile_def = new FileWriter(new File(outputPath, PROFILE_DEF));
                FileWriter profile_dat = new FileWriter(new File(outputPath, PROFILE_DAT));
                FileReader original = new FileReader(l.getFile(NETWORK_NTW), Charset.forName("BIG5"));
                BufferedReader input = new BufferedReader(original);
                FileWriter output = new FileWriter(new File(outputPath, NETWORK_NTW), StandardCharsets.UTF_8);) {
            network_cr.write("CR_1.1\n");
            for (Iterator<CrossSection> i = crossSections.iterator(); i.hasNext();) {
                CrossSection c = i.next();
                Map<String, String> records = Profile.createLitRecords(c, network);
                if (records.containsKey("BRANCH-TO-UPDATE")) {
                    branchesToUpdate.put(records.get("BRANCH-TO-UPDATE"), c);
                }
                LOGGER.info(" CRSN record NETWORK.CR:\n" + records.get(NETWORK_CR));
                LOGGER.info(" CRSN record PROFILE.DAT:\n" + records.get(PROFILE_DAT));
                LOGGER.info(" CRDS record:\n" + records.get(PROFILE_DEF));
                network_cr.write(records.get(NETWORK_CR));
                profile_def.write(records.get(PROFILE_DEF));
                profile_dat.write(records.get(PROFILE_DAT));
            }
            String line = null;
            boolean check = true;
            while ((line = input.readLine()) != null) {
                if (check) {
                    String firstPart = Stream.of(line.split("[\",]+")).filter(p -> p != null && p.length() > 0)
                            .findFirst().orElse(null);
                    check = !("*".equals(firstPart));
                    if (branchesToUpdate.containsKey(firstPart)) {
                        NetworkNTW.Branch raw = NetworkNTW.buildBranch(line);
                        Network.Branch branch = Network.Branch.fromNetworkNTW(raw);
                        LengthIndexedLine branchLine = new LengthIndexedLine(branch.toLineString());
                        List<CrossSection> sorted = ((List<CrossSection>) branchesToUpdate.get(firstPart)).stream()
                                .sorted(Comparator
                                        .comparingDouble(c -> branchLine.project(c.referencePoint().getCoordinate())))
                                .collect(Collectors.toList());
                        NetworkNTW.Branch toWrite = NetworkNTW.buildBranch(line);
                        for (int i = 0; i < sorted.size(); i++) {
                            CrossSection c = sorted.get(i);
                            Coordinate toPoint = c.getReferencePoint().getCoordinate();
                            toWrite.NdToID = c.getGeoId();
                            toWrite.NdToName = c.getGeoId();
                            toWrite.NdToObjID = "SBK_PROFILE";
                            toWrite.NdToX = BigDecimal.valueOf(toPoint.x);
                            toWrite.NdToY = BigDecimal.valueOf(toPoint.y);
                            output.write(NetworkNTW.branchLine(toWrite));
                            output.write("\n");
                            toWrite.BrID = raw.BrID + "__" + i;
                            toWrite.NdFrmID = toWrite.NdToID;
                            toWrite.NdFrmName = toWrite.NdToName;
                            toWrite.NdFrmObjID = toWrite.NdToObjID;
                            toWrite.NdFrmX = toWrite.NdToX;
                            toWrite.NdFrmY = toWrite.NdToY;
                        }
                        toWrite.NdToID = raw.NdToID;
                        toWrite.NdToName = raw.NdToName;
                        toWrite.NdToObjID = raw.NdToObjID;
                        toWrite.NdToX = raw.NdToX;
                        toWrite.NdToY = raw.NdToY;
                        output.write(NetworkNTW.branchLine(toWrite));
                        output.write("\n");
                        continue;
                    }
                }
                output.write(line);
                output.write("\n");
            }
        } catch (Exception ie) {
            ie.printStackTrace();
        }

        // Map<String, Profile> profiles = Profile.load(l);
        // List<CrossSection> litCrossSections = profiles.values().stream().filter(p ->
        // {
        // LitRecord d = p.child(LitDataType.PROFILE.CRDS);
        // return d != null && d.hasAttribute("yz");
        // }).map(p -> p.resolveNetwork(network::findNode, network::findBranch,
        // network::findReach))
        // .map(Profile::toCrossSection).filter(f -> f !=
        // null).collect(Collectors.toList());
    }
}

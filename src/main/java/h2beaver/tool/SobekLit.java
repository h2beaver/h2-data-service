package h2beaver.tool;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import org.apache.commons.io.FileUtils;

import h2beaver.smm.model.CrossSection;
import h2beaver.smm.utils.CrossSectionUtils;
import h2beaver.sobek.lit.FileLoader;
import h2beaver.sobek.lit.LitDataType;
import h2beaver.sobek.lit.LitRecord;
import h2beaver.sobek.lit.Network;
import h2beaver.sobek.lit.Profile;
import h2beaver.sobek.lit.Common;
import tw.fondus.commons.json.util.gson.GsonMapperRuntime;

public class SobekLit implements Common {

    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            System.err.println(
                    "USAGE: java -cp h2beaver-fat.jar h2beaver.tool.SobekLit {project} {lit-path} {git-project-path}");
            System.err.println();
            System.err.println(
                    "Example: java -cp h2beaver-fat.jar h2beaver.tool.SobekLit yilan ./yilan_demo/YL_TC_DEMO/YL_TC_D.lit/27 ./git-repos/");
            System.exit(-1);
        }
        String region = args[0];
        FileLoader l = new FileLoader(Paths.get(args[1]));
        Network network = Network.load(l);
        Path outputStreamNetwork = Paths.get(args[2], Common.getRepoName(region), "data");
        Files.createDirectories(outputStreamNetwork);
        FileUtils.writeStringToFile(new File(outputStreamNetwork.toFile(), "channel-network.geojson"),
                network.getStreamNetworkGeojson(), "UTF-8");
        LOGGER.info("Channel-network generated");
        FileUtils.writeStringToFile(new File(outputStreamNetwork.toFile(), "nodes.geojson"),
                network.getAllNodesGeojson(), "UTF-8");
        LOGGER.info("All nodes generated");

        Map<String, Profile> profiles = Profile.load(l);
        List<CrossSection> crossSections = profiles.values().stream().filter(p -> {
            LitRecord d = p.child(LitDataType.PROFILE.CRDS);
            return d != null && d.hasAttribute("yz");
        }).map(p -> p.resolveNetwork(network)).map(Profile::toCrossSection).filter(f -> f != null)
                .collect(Collectors.toList());

        Path outputCrossSectionResources = Paths.get(args[2], Common.getRepoName(region), "src", "main",
                "resources");
        Files.createDirectories(outputCrossSectionResources);
        FileUtils.cleanDirectory(outputCrossSectionResources.toFile());
        Gson gson = GsonMapperRuntime.GEOJSON.getGson().newBuilder().setPrettyPrinting().create();
        crossSections.forEach(c -> {
            if (c != null) {
                String fileName = c.getGeoId() + ".json";
                LOGGER.info("Export profile file: " + fileName);
                try {
                    FileUtils.writeStringToFile(new File(outputCrossSectionResources.toFile(), fileName),
                            gson.toJson(c.asGeoJsonFeatureCollection()), StandardCharsets.UTF_8);
                } catch (IOException e) {
                    LOGGER.warn("Fail to export: " + fileName, e);
                }
            }
        });

        Path outputCrossSectionData = Paths.get(args[2], Common.getRepoName(region), "data");
        try {
            LOGGER.info("Export profile file: cross-section.geojson");
            FileUtils.writeStringToFile(new File(outputCrossSectionData.toFile(), "cross-section.geojson"),
                    gson.toJson(CrossSectionUtils.getMergedPointFeatureGeojson(crossSections)), StandardCharsets.UTF_8);
            LOGGER.info("Export profile file: cross-section-xyz.geojson");
            FileUtils.writeStringToFile(new File(outputCrossSectionData.toFile(), "cross-section-xyz.geojson"),
                    gson.toJson(CrossSectionUtils.getMergedLineStringGeojson(crossSections)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.warn("Fail to export: cross-section.geojson", e);
        }
    }
}

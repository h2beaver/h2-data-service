package h2beaver.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import h2beaver.demo.data.ProfileMetadata;
import h2beaver.service.crossSection.Profile;

public class CrossSectionProcess {
    private static Logger logger = LoggerFactory.getLogger("SOBEK-LIT-TOOL");

    public static void showUsage(String errorMessage) {
        System.err.println(errorMessage);
        System.err.println(
                "USAGE: java -cp h2beaver-fat.jar h2beaver.tool.CrossSectionProcess {project} {excel-xy} {excel-yz} {git-project-path}");
        System.err.println();
        System.err.println(
                "Example: java -cp h2beaver-fat.jar h2beaver.tool.CrossSectionProcess yilan 宜蘭斷面座標一覽表.xls 宜蘭.xls /tmp/git-test");
        System.exit(-1);

    }

    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
            showUsage("");
        }
        String region = args[0];

        String projectName = "cross_section-" + region + "-vector";

        File f1 = new File(args[1]);
        if (!f1.exists()) {
            showUsage(f1.getName() + " doesn't exists");
        }
        File f2 = new File(args[2]);
        if (!f2.exists()) {
            showUsage(f2.getName() + " doesn't exists");
        }

        Path outputCrossSectionProject = Paths.get(args[3], projectName);
        File uploadFolder = new File(outputCrossSectionProject.toFile(), "upload");
        Profile profile = Profile.getInstance(outputCrossSectionProject.toString());
        ProfileMetadata meta = new ProfileMetadata();
        meta.setProjectName(projectName);
        meta.setFilename1(f1.getName());
        meta.setFilename2(f2.getName());
        try (FileInputStream profileNodeInputStream = FileUtils.openInputStream(f1);
                FileInputStream profileDataInputStream = FileUtils.openInputStream(f2)) {
            String branch = profile.processExcelSheets(meta, profileNodeInputStream, profileDataInputStream);
            logger.info("Excel process finished");
            profile.checkoutBranch(branch);
            logger.info("Please check the file under the folder: " + uploadFolder.getCanonicalPath());
        } catch (IOException e) {
            logger.warn("Fail to process uploaded Excel", e);
        }
        ;
    }
}

package h2beaver.fondus;

import java.util.Collections;

import tw.fondus.commons.json.geojson.model.crs.CoordinateReferenceSystem;

public class CRS {
    public static CoordinateReferenceSystem fromEPSG_SRID(int SRID) {
        return CoordinateReferenceSystem.builder()
                .properties(Collections.singletonMap("name", "urn:ogc:def:crs:EPSG::" + SRID)).build();
    }
}

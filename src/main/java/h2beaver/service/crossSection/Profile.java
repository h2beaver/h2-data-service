package h2beaver.service.crossSection;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.math.Vector2D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import h2beaver.demo.data.ProfileMetadata;
import h2beaver.service.GitBase;
import h2beaver.smm.model.CrossSection;
import h2beaver.smm.utils.CrossSectionUtils;
import h2beaver.sobek.lit.Common;
import tw.fondus.commons.json.util.gson.GsonMapperRuntime;

public class Profile extends GitBase {
    private static Logger log = LoggerFactory.getLogger("H2BEAVER");
    private static Map<String, Profile> instanceMap = new ConcurrentHashMap<>();

    private static Profile init(String path) {
        try {
            return new Profile(path);
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    public static Profile getInstance(String path) {
        return instanceMap.computeIfAbsent(path, k -> init(k));
    }

    protected Profile(String path) throws IOException {
        super(path);
    }

    private void setProfileLeftRightPoint(Row r, CrossSection p) {
        if ("L".equals(r.getCell(1).getStringCellValue())) {
            p.setStartPoint(Common.TWD97.createPoint(new Coordinate(r.getCell(4).getNumericCellValue(),
                    r.getCell(3).getNumericCellValue(), r.getCell(5).getNumericCellValue())));
        } else if ("R".equals(r.getCell(1).getStringCellValue())) {
            p.setEndPoint(Common.TWD97.createPoint(new Coordinate(r.getCell(4).getNumericCellValue(),
                    r.getCell(3).getNumericCellValue(), r.getCell(5).getNumericCellValue())));
        }
    }

    // Example of 2-rows profile data
    // NK-H01320-04260C	L	7623.69	2753581.75	275776.1875	185.6	2753571.9759	275778.6584	2018
    //	                R	7623.69	2753562.25	275781.1251	185.6	2753571.9759	275778.6584	2018
    private Map<String, CrossSection> getStreamAndProfilePoint(Sheet dataSheet) {
        Map<String, CrossSection> data = new LinkedHashMap<>();
        Cell A1 = dataSheet.getRow(0).getCell(0);
        if ("斷面編號".equals(A1.getStringCellValue())) {
            log.info("Data sheet loaded: " + dataSheet.getSheetName());
            for (int i = 2; i <= 5000; i += 2) {
                Map<String, Object> properties = new LinkedHashMap<>();
                Row r1 = dataSheet.getRow(i);
                Row r2 = dataSheet.getRow(i + 1);
                if (r1 == null || r2 == null)
                    break;
                String id = r1.getCell(0).getStringCellValue();
                CrossSection profile = CrossSection.builder().id(id).build();
                // Setup points
                setProfileLeftRightPoint(r1, profile);
                setProfileLeftRightPoint(r2, profile);
                profile.setReferencePoint(Common.TWD97.createPoint(new Coordinate(r1.getCell(7).getNumericCellValue(),
                        r1.getCell(6).getNumericCellValue(), r1.getCell(5).getNumericCellValue())));

                // Setup properties
                properties.put(CrossSection.PROPERITY_NAME.distanceFromReferent.name(),
                        r1.getCell(2).getNumericCellValue());
                Cell noteCell = r1.getCell(8);
                switch (noteCell.getCellType()) {
                    case STRING:
                        properties.put("note", noteCell.getStringCellValue());
                        break;
                    case NUMERIC:
                        properties.put("note", Double.toString(noteCell.getNumericCellValue()));
                        break;
                    default:
                }
                profile.setProperties(properties);

                data.put(id, profile);
                log.info("Row loaded: " + i + "," + id);
            }
        }
        return data;
    }

    private void findAndUpdateProfileYZ(ProfileMetadata meta, Map<String, CrossSection> data, Sheet dataSheet) {
        Cell A1 = dataSheet.getRow(0).getCell(0);
        String referent = A1.getStringCellValue();
        log.info("Data sheet loaded: " + dataSheet.getSheetName() + " for " + referent);
        int count = 0;
        for (int i = 1; i < 5000; i++) {
            Row r = dataSheet.getRow(i);
            if (r == null)
                continue;
            Cell c = r.getCell(0);
            if (c == null || c.getCellType() == CellType._NONE || c.getCellType() == CellType.BLANK)
                break;
            String no = c.getStringCellValue();
            log.info("Profile data " + no + " found at row: " + i);
            CrossSection profile = data.get(no);
            profile.getProperties().put(CrossSection.PROPERITY_NAME.referentId.name(), referent);
            int rows = (int) r.getCell(2).getNumericCellValue();
            List<Number[]> table = new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                i++;
                Row d = dataSheet.getRow(i);
                if (d == null) {
                    log.warn("Profile data " + no + " row number mismatch, at row: " + i);
                    break;
                }
                Cell c1 = d.getCell(1);
                Cell c2 = d.getCell(2);
                table.add(new Number[] { c1.getNumericCellValue(), c2.getNumericCellValue() });
            }
            if (profile != null) {
                count++;
                log.info("Profile YZ data loaded for " + no);
                profile.getProperties().put("profile", table.toArray(new Number[][] {}));
            }

            int numProfilePoints = table.size();
            double profileY0 = table.get(0)[0].doubleValue();
            Vector2D direction = Vector2D
                    .create(profile.getStartPoint().getCoordinate(), profile.getEndPoint().getCoordinate()).normalize();
            Coordinate[] profileCoordinates = new Coordinate[numProfilePoints];
            Coordinate profileStartPoint = profile.getStartPoint().getCoordinate();
            profileCoordinates[0] = profileStartPoint;
            for (int p = 1; p < numProfilePoints; p++) {
                Number[] yz = table.get(p - 1);
                Vector2D pointReference = direction.multiply(yz[0].doubleValue() - profileY0);
                profileCoordinates[p] = new Coordinate(profileStartPoint.x + pointReference.getX(),
                        profileStartPoint.y + pointReference.getY(), yz[1].doubleValue());
            }
            LineString profileFeature = Common.TWD97.createLineString(profileCoordinates);
            profile.setFeature(profileFeature);
        }
        meta.regions.put(A1.getStringCellValue(), count);
    }

    public String processExcelSheets(ProfileMetadata meta, InputStream profileNodeInputStream,
            InputStream profileDataInputStream) throws IOException, GitAPIException {
        Map<String, CrossSection> data = new LinkedHashMap<>();
        try (Workbook wb = WorkbookFactory.create(profileNodeInputStream);) {
            log.info("座標檔 " + meta.getFilename1() + " has " + wb.getNumberOfSheets() + " sheets;");
            for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                data.putAll(getStreamAndProfilePoint(wb.getSheetAt(i)));
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException("座標", ex);
        }

        try (Workbook wb = WorkbookFactory.create(profileDataInputStream);) {
            log.info("斷面檔" + meta.getFilename2() + " has " + wb.getNumberOfSheets() + " sheets;");
            for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                findAndUpdateProfileYZ(meta, data, wb.getSheetAt(i));
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException("斷面", ex);
        }

        String branch = addNewProfiles(meta, data);
        return branch;
    }

    public String addNewProfiles(ProfileMetadata meta, Map<String, CrossSection> data)
            throws IOException, GitAPIException {
        ObjectMapper mapper = new ObjectMapper();
        String branch = "b" + System.currentTimeMillis();
        Git git = new Git(this.repository);
        checkoutBranch("master");

        File upload = new File(this.repository.getWorkTree(), "upload");
        upload.mkdir();

        git.checkout().setCreateBranch(true).setName(branch).call();
        Gson gson = GsonMapperRuntime.GEOJSON.getGson().newBuilder().setPrettyPrinting().create();

        mapper.writeValue(new File(upload, "metadata.json"), meta);
        for (Iterator<Map.Entry<String, CrossSection>> iterator = data.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, CrossSection> e = iterator.next();
            String fileName = e.getKey() + ".json";
            log.info("Export profile file: " + fileName);
            try {
                FileUtils.writeStringToFile(new File(upload, fileName),
                        gson.toJson(e.getValue().asGeoJsonFeatureCollection()), StandardCharsets.UTF_8);
            } catch (IOException ex) {
                log.warn("Fail to export: " + fileName, ex);
            }
        }
        git.add().addFilepattern("upload/").call();
        git.commit().setMessage("Commit uploaded excel data - " + branch).call();

        log.info("Export profile file: cross-section.geojson");
        FileUtils.writeStringToFile(new File(upload, "upload.geojson"),
                gson.toJson(CrossSectionUtils.getMergedPointFeatureGeojson(data.values())), StandardCharsets.UTF_8);
        log.info("Export profile file: cross-section-xyz.geojson");
        FileUtils.writeStringToFile(new File(upload, "upload-xyz.geojson"),
                gson.toJson(CrossSectionUtils.getMergedLineStringGeojson(data.values())), StandardCharsets.UTF_8);

        git.add().addFilepattern("upload/").call();
        git.commit().setMessage("Commit merged layer geojsion - " + branch).call();

        pushBranch(null);
        checkoutBranch("master");

        return branch;
    }
}

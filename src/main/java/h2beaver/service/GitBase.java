package h2beaver.service;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import h2beaver.demo.Settings;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;
import org.eclipse.jgit.util.FS;

import java.io.IOException;
import java.nio.file.Paths;

public abstract class GitBase {
    private static TransportConfigCallback configCallback = new TransportConfigCallback() {
        @Override
        public void configure(final Transport transport) {
            SshTransport sshTransport = (SshTransport) transport;
            sshTransport.setSshSessionFactory(new JschConfigSessionFactory() {

                @Override
                protected JSch getJSch(final OpenSshConfig.Host hc, FS fs) throws JSchException {
                    JSch jsch = super.getJSch(hc, fs);
                    jsch.removeAllIdentity();
                    jsch.addIdentity(Settings.GIT_SSH_FILE, Settings.GIT_SSH_PASSPHRASE);
                    return jsch;
                }

                @Override
                protected void configure(final OpenSshConfig.Host hc, final Session session) {
                    session.setConfig("StrictHostKeyChecking", "no");
                }
            });
        }
    };

    protected final Repository repository;
    protected final Git git;
    protected final boolean hasRemoteOrigin;

    private static Repository repositoryFromPath(String path) throws IOException {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repository = builder.findGitDir(Paths.get(path).toFile())
                .readEnvironment() // scan environment GIT_* variables
                .build();
        return repository;
    }

    protected GitBase(Repository repository) {
        this.repository = repository;
        this.git = new Git(this.repository);
        String url = repository.getConfig().getString("remote", "origin", "url");
        hasRemoteOrigin = url != null;
    }

    protected GitBase(String path) throws IOException {
        this(repositoryFromPath(path));
    }

    public void checkoutBranch(String branch) throws GitAPIException {
        git.checkout().setName(branch).call();
        if (hasRemoteOrigin) git.pull().setTransportConfigCallback(configCallback).call();
    }

    public void pushBranch(String branch) throws GitAPIException {
        if (branch != null) git.checkout().setName(branch).call();
        if (hasRemoteOrigin) git.push().setTransportConfigCallback(configCallback).call();
    }
}

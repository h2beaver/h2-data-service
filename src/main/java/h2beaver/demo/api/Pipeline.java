package h2beaver.demo.api;

import h2beaver.demo.Settings;
import h2beaver.demo.data.DataVersion;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

@Path("pipeline")
public class Pipeline {
    private static String PROJECT_TARGET = "https://h2-gitlab.pointing.tw/api/v4/projects/11";
    private static String HEADER_TOKEN = "PRIVATE-TOKEN";

    @Context
    HttpServletResponse response;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public DataVersion[] getPipelines(@QueryParam("per_page") Integer per_page) {
        int per_page_value = Optional.ofNullable(per_page).orElse(Integer.valueOf(10)).intValue();

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PROJECT_TARGET);
        Map[] r = webTarget.path("pipelines").queryParam("per_page", per_page_value).queryParam("scope", "branches")
                .queryParam("status", "success").request(MediaType.APPLICATION_JSON)
                .header(HEADER_TOKEN, Settings.GITLAB_ACCESS_TOKEN).get(Map[].class);
        if (r.length > 0) {
            return Arrays.stream(r).map(v -> getDataVersion(v)).toArray(DataVersion[]::new);
        }
        response.setStatus(404);
        return null;
    }

    @GET
    @Path("status/{ref}")
    @Produces(MediaType.APPLICATION_JSON)
    public DataVersion getPipelineStatus(@PathParam("ref") String ref) {
        DataVersion v = getDataVersion(getPipeline(ref));
        if (v != null)
            return v;
        response.setStatus(404);
        return null;
    }

    private Map getPipeline(String ref) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PROJECT_TARGET);
        Map[] r = webTarget.path("pipelines").queryParam("ref", ref).queryParam("per_page", 1)
                .request(MediaType.APPLICATION_JSON).header(HEADER_TOKEN, Settings.GITLAB_ACCESS_TOKEN)
                .get(Map[].class);
        if (r.length > 0)
            return r[0];
        return null;
    }

    private DataVersion getDataVersion(Map pipelineData) {
        if (pipelineData == null)
            return null;

        DataVersion v = DataVersion.fromPipelineData(pipelineData);
        return v.withProperty("streamReleaseVersions", Settings.STREAM_RELEASE_VERSIONS)
                .withProperty("profileReleaseVersions", Settings.PROFILE_RELEASE_VERSIONS)
                .withProperty("streamReleaseDownloadInfos", Settings.STREAM_RELEASE_DOWNLOAD_INFOS)
                .withProperty("profileReleaseDownloadInfos", Settings.PROFILE_RELEASE_DOWNLOAD_INFOS);
    }

    @GET
    @Path("geojson/{ref}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getPipelineGeojson(@PathParam("ref") String ref) {
        Map r = getPipeline(ref);
        if (r == null)
            return null;
        if ("success".equals(r.get("status"))) {
            Client client = ClientBuilder.newClient();
            WebTarget webTarget = client.target(PROJECT_TARGET);
            return webTarget.path("jobs/artifacts/" + ref + "/raw/workspace/uploaded.geojson")
                    .queryParam("job", "build").request().header(HEADER_TOKEN, Settings.GITLAB_ACCESS_TOKEN)
                    .get(String.class);
        } else {
            DataVersion v = getDataVersion(r);
        }
        return null;
    }
}

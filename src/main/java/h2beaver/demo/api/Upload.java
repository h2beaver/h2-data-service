package h2beaver.demo.api;

import h2beaver.demo.Settings;
import h2beaver.demo.data.DataVersion;
import h2beaver.demo.data.ProfileMetadata;
import org.apache.poi.EmptyFileException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


@Path("/")
public class Upload {
    private static Logger log = LoggerFactory.getLogger("H2BEAVER");

    private void generateErrorMessage(String fileType, String filename, Exception ex) {
        log.warn("Excel parsing error " + fileType, ex);
        StringBuilder message = new StringBuilder(filename);
        message.append(fileType);
        message.append("檔案讀取錯誤：");
        if (ex instanceof GitAPIException) {
            message.append("資料寫入暫存區失敗，請連繫系統管理員");
        } else if (ex instanceof EmptyFileException || ex instanceof IOException) {
            message.append("無法判讀為 Excel 檔案格式");
        } else {
            message.append("無法自檔案中正確擷取");
            message.append(fileType);
            message.append("資料");
        }

        Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST);
        builder.entity(new DataVersion(DataVersion.Status.Error, null, message.toString()));
        throw new WebApplicationException(builder.build());
    }

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public DataVersion uploadFile(
            @FormDataParam("project") String projectName,
            @FormDataParam("file1") InputStream profileNodeInputStream,
            @FormDataParam("file1") FormDataContentDisposition profileNodeDetails,
            @FormDataParam("file2") InputStream profileDataInputStream,
            @FormDataParam("file2") FormDataContentDisposition profileDataDetails) throws Exception {
        ProfileMetadata meta = new ProfileMetadata();
        meta.setProjectName(projectName);
        meta.setFilename1(profileNodeDetails.getFileName());
        meta.setFilename2(profileDataDetails.getFileName());

        if (meta.getFilename1()!=null && meta.getFilename2()!=null) {
            try {
                h2beaver.service.crossSection.Profile repo = h2beaver.service.crossSection.Profile.getInstance(projectName);
                String branch = repo.processExcelSheets(meta, profileNodeInputStream, profileDataInputStream);
                DataVersion v = new DataVersion(DataVersion.Status.OK, branch, new Date().toString());
                return v
                        .withProperty("streamReleaseVersions", Settings.STREAM_RELEASE_VERSIONS)
                        .withProperty("profileReleaseVersions", Settings.PROFILE_RELEASE_VERSIONS)
                        .withProperty("streamReleaseDownloadInfos", Settings.STREAM_RELEASE_DOWNLOAD_INFOS)
                        .withProperty("profileReleaseDownloadInfos", Settings.PROFILE_RELEASE_DOWNLOAD_INFOS);
            } catch (Exception ex) {
                generateErrorMessage("斷面", meta.getFilename1() + " / " + meta.getFilename2(), ex);
            }
        }
        return null;
    }
}

package h2beaver.demo;

import java.util.Optional;

public class Settings {
    public static final String REPO_BASEDIR = Optional.ofNullable(System.getenv("H2_REPO_BASEDIR")).orElse("/home/kao/h2beaver/demo/repos/");

    public static final String GIT_SSH_FILE = Optional.ofNullable(System.getenv("H2_GIT_SSH_FILE")).orElse("/home/kao/.ssh/id_rsa");
    public static final String GIT_SSH_PASSPHRASE = Optional.ofNullable(System.getenv("H2_GIT_SSH_PASSPHRASE")).orElse("sshkey");

    public static final String GITLAB_ACCESS_TOKEN = Optional.ofNullable(System.getenv("H2_GITLAB_ACCESS_TOKEN")).orElse(null);

    public static String STREAM_RELEASE_VERSIONS = "https://h2-archiva.pointing.tw/restServices/archivaServices/browseService/versionsList/h2beaver.taoyuan/stream_network-taoyuan-vector";
    public static String PROFILE_RELEASE_VERSIONS = "https://h2-archiva.pointing.tw/restServices/archivaServices/browseService/versionsList/h2beaver.taoyuan/profile-taoyuan-vector";
    public static String STREAM_RELEASE_DOWNLOAD_INFOS = "https://h2-archiva.pointing.tw/restServices/archivaServices/browseService/artifactDownloadInfos/h2beaver.taoyuan/stream_network-taoyuan-vector/";
    public static String PROFILE_RELEASE_DOWNLOAD_INFOS = "https://h2-archiva.pointing.tw/restServices/archivaServices/browseService/artifactDownloadInfos/h2beaver.taoyuan/profile-taoyuan-vector/";

}

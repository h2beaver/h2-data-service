package h2beaver.demo.data;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProfileMetadata {
    public final Date whenUpload = new Date();
    public final Map<String, Integer> regions = new LinkedHashMap<>();
    private String projectName;
    private String filename1;
    private String filename2;


    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getFilename1() {
        return filename1;
    }

    public void setFilename1(String filename1) {
        this.filename1 = filename1;
    }

    public String getFilename2() {
        return filename2;
    }

    public void setFilename2(String filename2) {
        this.filename2 = filename2;
    }

}

package h2beaver.demo.data;

import java.util.ArrayList;
import java.util.List;

public class PipelineVariables {
    public String ref;
    public List<Variable> variables;

    public PipelineVariables() {
        this.variables = new ArrayList();
    }

    public static class Variable {
        public String key;
        public String value;

        public Variable() {}
        public Variable(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}

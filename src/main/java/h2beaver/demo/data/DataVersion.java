package h2beaver.demo.data;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.Properties;

public class DataVersion {
    public enum Status {
        OK, Error, Processing, Canceled, Failed
    }

    public final Status status;
    public final String version;
    public final String createdAt;
    public final String message;
    public final Properties properties;
    public DataVersion(Status status, String version, String createdAt) {
        this(status, version, createdAt, null);
    }
    public DataVersion(Status status, String version, String createdAt, String message) {
        this.status = status;
        this.version = version;
        this.createdAt = createdAt;
        this.message = message;
        this.properties = new Properties();
    }

    public DataVersion withProperty(String key, String value) {
        this.properties.setProperty(key, value);
        return this;
    }

    static public DataVersion fromPipelineData(Map pipelineData) {
        if (pipelineData == null) return null;

        String status = (String) pipelineData.get("status");
        String ref = (String) pipelineData.get("ref");
        String createdAt = (String) pipelineData.get("created_at");
        switch (status) {
            case "success":
                return new DataVersion(Status.OK, ref, createdAt);
            case "pending":
            case "running": {
                Response.ResponseBuilder builder = Response.status(206);
                builder.entity(new DataVersion(Status.Processing, ref, createdAt));
                throw new WebApplicationException(builder.build());
            }
            case "canceled": {
                Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST);
                builder.entity(new DataVersion(Status.Canceled, ref, createdAt, "管理員中斷計算作業"));
                throw new WebApplicationException(builder.build());
            }
            default: {
                Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST);
                builder.entity(new DataVersion(Status.Failed, ref, createdAt, "計算作業"));
                throw new WebApplicationException(builder.build());
            }
        }
    }
}

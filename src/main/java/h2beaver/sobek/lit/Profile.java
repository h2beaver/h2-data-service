package h2beaver.sobek.lit;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineSegment;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.linearref.LengthIndexedLine;
import org.locationtech.jts.linearref.LinearLocation;
import org.locationtech.jts.linearref.LocationIndexedLine;
import org.locationtech.jts.math.Vector2D;

import h2beaver.smm.model.CrossSection;
import tw.fondus.commons.json.geojson.model.feature.Feature;
import tw.fondus.commons.json.util.gson.GsonMapperRuntime;
import tw.fondus.commons.spatial.util.geojson.GeoJsonMapper;

public class Profile extends LitRecord implements Common {
    public static final List<String> FILES = Arrays.asList("profile.def", "network.cr", "profile.dat");

    public enum PROFILE_DEF_TYPE {
        UNKNOWN(-1), TABULATED(0), TRAPEZIUM(1), OPEN_CIRCLE(2), SEDREDGE(3), CLOSED_CIRCLE(4), EGG_SHAPED(6),
        EGG_SHAPED_2(7), CLOSED_RECTANGULAR(8), YZ_TABLE(10), ASYMMETRICAL_TRAPEZIODAL(11);

        private final int type_id;

        private PROFILE_DEF_TYPE(int type_id) {
            this.type_id = type_id;
        }

        public static PROFILE_DEF_TYPE getByTypeId(int id) {
            return Stream.of(PROFILE_DEF_TYPE.values()).filter(p -> p.type_id == id).findAny().orElse(UNKNOWN);
        }
    }

    protected Network.Node node;
    protected Network.Reach reach;
    protected Network.Branch branch;
    private LitDataType.PROFILE profileDataType;
    private PROFILE_DEF_TYPE profileDefType;
    private Number[][] profileYZ;

    public Profile(String source, LitDataType.PROFILE type, LitRecord record) {
        super(source, type, record.getId());
        this.profileDataType = type;
        this.attributes.putAll(record.attributes);
        this.children.putAll(record.children);
        this.values.addAll(record.values);
        if (profileDataType == LitDataType.PROFILE.CRDS) {
            LitRecord table = this.children.get(LitDataType.getType("TBLE"));
            profileYZ = table == null ? new Number[][] { { 0, 0 } } : table.numberValueAsArray();
        }
    }

    public Network.Node node() {
        return node;
    }

    public Network.Branch branch() {
        return branch;
    }

    public Network.Reach reach() {
        return reach;
    }

    @Override
    protected void valueUpdated() {
        if (this.attributes.containsKey("ty")) {
            this.profileDefType = PROFILE_DEF_TYPE
                    .getByTypeId(this.getAttributeValue("ty").getNumberValue().intValue());
        }
    }

    public PROFILE_DEF_TYPE defType() {
        return this.profileDefType;
    }

    public Number[][] getYZTable() {
        if (profileYZ == null) {
            Profile crds = (Profile) this.children.get(LitDataType.PROFILE.CRDS);
            if (crds != null) {
                this.profileYZ = crds.profileYZ;
            }
        }
        return this.profileYZ;
    }

    public Profile resolveNetwork(Network network) {
        if (LitDataType.PROFILE.CRSN == this.profileDataType) {
            Network.Node n = network.findNode(this.getId());
            if (n != null && n.nodeType() == Network.SOBEK_NODE_TYPE.SBK_PROFILE) {
                LOGGER.info("Link profile " + this.getId() + " to profile node: " + n.getId());
                this.node = n;
                this.linkTo(n, true);
            }
        }
        if (LitDataType.PROFILE.CRSN == this.profileDataType && this.attributes.containsKey("ci")) {
            Network.Branch branch = network.findBranch(this.getStringAttribute("ci"));
            if (branch != null) {
                this.branch = branch;
                if (this.node == null) {
                    LOGGER.info("Link profile " + this.getId() + " to branch node 0: " + branch.begin.getId());
                    this.node = branch.begin;
                }
                this.linkTo(branch.begin, true);
            }
        }
        if (LitDataType.PROFILE.CRSN == this.profileDataType && this.attributes.containsKey("ci")) {
            Network.Reach reach = network.findReach(this.getStringAttribute("ci"));
            if (reach != null) {
                this.reach = reach;
                LOGGER.info("Profile belongs to reach: " + this.reach.getId());
                if (this.node == null) {
                    LOGGER.info("Link profile " + this.getId() + " to nearest reach node: ");
                }
            } else {
                LOGGER.warn("Profile reach is unknown");
            }

        }
        if (this.node == null) {
            LOGGER.warn("Profile node is unknown");
            return this;
        }
        if (this.reach == null && this.node.reach() != null) {
            this.reach = this.node.reach();
            LOGGER.info("Profile belongs to reach via node: " + this.reach.getId());
        }
        return this;
    }

    public static Map<String, String> createLitRecords(CrossSection cs, Network network) {
        Map<String, String> records = new HashMap<>();

        LineString feature = cs.getFeature();
        LengthIndexedLine lengthIndexedLine = new LengthIndexedLine(feature);
        Map<String, Object> properties = cs.getProperties();

        String sobek_id = (String) properties.get("sobek_id");
        Point referencePoint = cs.referencePoint();
        double referenceShift = lengthIndexedLine.indexOf(referencePoint.getCoordinate());
        Network.Node node = network.findNode(sobek_id);
        if (node == null)
            node = network.findNearestNode(referencePoint, 1.0);
        String nodeId = node == null ? cs.getGeoId() : node.getId();
        Network.Branch branch = network.findNearestBranch(referencePoint, 1.0);
        Network.Reach reach = (node != null && node.reach() != null) ? node.reach()
                : network.findNearestReach(referencePoint, 1.0);

        if (node == null && branch != null && reach != null) {
            records.put("BRANCH-TO-UPDATE", branch.getId());
            LineString reachLineString = reach.toLineString();
            LengthIndexedLine reachLengthIndexedLine = new LengthIndexedLine(reachLineString);
            System.err.println(reachLengthIndexedLine.indexOf(referencePoint.getCoordinate()));
            System.err.println(branch);
            System.err.println(reach);
        }

        LitRecordWriter crsnNetwork = LitRecordWriter.of("CRSN", properties).addValue("id", nodeId)
                .addPropertyValue("nm", CrossSection.PROPERITY_NAME.name.name())
                .addPropertyValue("ci", CrossSection.PROPERITY_NAME.referentId.name())
                .addPropertyValue("lc", CrossSection.PROPERITY_NAME.distanceFromReferent.name());
        records.put("NETWORK.CR", crsnNetwork.build());

        LitRecordWriter crsnProfile = LitRecordWriter.of("CRSN", properties).addValue("id", nodeId)
                .addValue("di", cs.getGeoId()).optionalValue("fg").optionalValue("rl").optionalValue("ll")
                .optionalValue("rs").optionalValue("ls");
        records.put("PROFILE.DAT", crsnProfile.build());

        LitRecordWriter crdsWriter = LitRecordWriter.of("CRDS", properties).addValue("id", cs.getGeoId())
                .addPropertyValue("nm", CrossSection.PROPERITY_NAME.name.name()).optionalValue("ty").optionalValue("lu")
                .optionalValue("st").addValue("lt", null).addPropertyValue("sw").addValue("gl", 0).addValue("gu", 0)
                .addValue("lt", null).addValue("yz", null).addTable(Stream.of(feature.getCoordinates()).map(c -> {
                    double y = lengthIndexedLine.indexOf(c);
                    double z = c.getZ();
                    return new Object[] { y - referenceShift, z };
                }).toArray(Object[][]::new));
        records.put("PROFILE.DEF", crdsWriter.build());
        return records;
    }

    protected static String propertyMapping(String litName) {
        switch (litName) {
            case "id":
                return "sobek_id";
            // CRSN
            case "ci":
                return CrossSection.PROPERITY_NAME.referentId.name();
            case "lc":
                return CrossSection.PROPERITY_NAME.distanceFromReferent.name();
            // CRDS
            case "nm":
                return CrossSection.PROPERITY_NAME.name.name();
            default:
                return litName;
        }
    }

    protected Map<String, Object> getFeatureProperties() {
        Map<String, Object> properties = new HashMap<>();
        if (this.node != null) {
            properties.put("node_id", this.node.getId());
            this.node.attributes.forEach((k, v) -> properties.put("node_" + k, v.getValue()));
        }
        this.attributes.forEach((k, v) -> properties.put(propertyMapping(k), v.getValue()));
        Profile crds = (Profile) this.children.get(LitDataType.PROFILE.CRDS);
        if (crds != null) {
            crds.attributes.forEach((k, v) -> properties.put(propertyMapping(k), v.getValue()));
        }
        if (this.profileYZ != null)
            properties.put("profile", this.getYZTable());

        properties.replaceAll((k, v) -> {
            if (v instanceof Number) {
                return SCALE_10000.makePrecise(((Number) v).doubleValue());
            } else
                return v;
        });
        return properties;
    }

    public Feature toGeojsonPointFeature() {
        if (this.node == null)
            return null;
        Map<String, Object> properties = getFeatureProperties();
        Point p = this.node.toPoint();
        Number[][] profile = this.getYZTable();

        Point p3d = profile == null ? p
                : TWD97.createPoint(new Coordinate(p.getX(), p.getY(), profile[0][1].doubleValue()));
        return Feature.builder().geometry(GeoJsonMapper.fromJTS(p3d)).id(this.getId()).properties(properties).build();
    }

    public CrossSection toCrossSection() {
        Number[][] profile = this.getYZTable();
        if (profile == null) {
            return null;
        }
        Network.Reach r = this.reach();
        if (r == null)
            return null;

        Map<String, Object> properties = getFeatureProperties();
        int numProfilePoints = profile.length;
        double profileY0 = profile[0][0].doubleValue();
        double profileLength = profile[numProfilePoints - 1][0].doubleValue() - profileY0;

        LineString l = r.toLineString();
        Point p;
        Coordinate c;
        if (this.node == null) {
            LengthIndexedLine lengthIndexedLine = new LengthIndexedLine(l);
            c = lengthIndexedLine.extractPoint(this.getNumberAttribute("lc").doubleValue());
            p = TWD97.createPoint(c);
        } else {
            p = this.node.toPoint();
            c = p.getCoordinate();
        }
        LocationIndexedLine locationIndexedLine = new LocationIndexedLine(l);
        LinearLocation location = locationIndexedLine.indexOf(c);
        LineSegment segment = location.getSegment(l);
        Vector2D direction = Vector2D.create(segment.p0, segment.p1).normalize().rotate(Math.PI / 2);
        Vector2D startPointReference = direction.multiply(profileLength / 2);
        Coordinate[] profileCoordinates = new Coordinate[numProfilePoints];
        Coordinate profileStartPoint = new Coordinate(c.getX() - startPointReference.getX(),
                c.getY() - startPointReference.getY(), profile[0][1].doubleValue());
        profileCoordinates[0] = profileStartPoint;
        for (int i = 1; i < numProfilePoints; i++) {
            Number[] yz = profile[i];
            Vector2D pointReference = direction.multiply(yz[0].doubleValue() - profileY0);
            profileCoordinates[i] = new Coordinate(profileStartPoint.x + pointReference.getX(),
                    profileStartPoint.y + pointReference.getY(), yz[1].doubleValue());
        }
        LineString profileFeature = TWD97.createLineString(profileCoordinates);

        return CrossSection.builder().id(this.getId()).name(this.getStringAttribute("nm"))
                .startPoint(profileFeature.getStartPoint()).endPoint(profileFeature.getEndPoint()).referencePoint(p)
                .properties(properties).feature(profileFeature).build();
    }

    public String getPointGeojson() {
        Feature f = this.toGeojsonPointFeature();
        return f == null ? null : GsonMapperRuntime.GEOJSON.toString(f);
    }

    public static Map<String, Profile> load(FileLoader loader) throws IOException {
        Map<String, Profile> profileDef = new LinkedHashMap<>();
        Map<String, Profile> profiles = new LinkedHashMap<>();
        for (Iterator<String> i = FILES.iterator(); i.hasNext();) {
            String file = i.next();
            List<LitRecord> records = loader.readFile(file);
            records.stream().forEach(record -> {
                try {
                    LitDataType type = record.getType();
                    String id = record.getId();
                    if (id != null && type instanceof LitDataType.PROFILE) {
                        Profile n = new Profile(file, (LitDataType.PROFILE) type, record);
                        if (LitDataType.PROFILE.CRDS == n.profileDataType) {
                            profileDef.put(id, n);
                            LOGGER.info("Profile loaded: " + n.getType() + "/" + n.defType() + "/" + n.getId());
                        } else {
                            Profile p = profiles.merge(id, n, (v1, v2) -> {
                                v1.merge(v2);
                                return v1;
                            });
                            // Assume DEF is loaded first
                            Profile crds = profileDef.remove(p.getStringAttribute("di"));
                            if (crds != null) {
                                p.children.put(crds.type, crds);
                            }
                            LOGGER.info("Profile updated: " + p.getType() + "/" + p.defType() + "/" + p.getId());
                        }
                    }
                } catch (Exception ex) {
                    LOGGER.error("Error when load Profile definitions", ex);
                }
            });
        }

        return profiles;
    }
}

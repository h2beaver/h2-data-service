package h2beaver.sobek.lit;

import java.util.HashMap;
import java.util.Map;

public interface LitDataType {
    public enum NETWORK implements LitDataType {
        NODE, BRCH, REACH
    }

    public enum PROFILE implements LitDataType {
        CRSN, CRDS, STON
    }

    static Map<String, LitDataType> knowTypes = new HashMap<>();

    public static LitDataType getType(String name) {
        if (name == null)
            return null;
            LitDataType type = knowTypes.get(name);
        if (type != null)
            return type;
        try {
            final LitDataType network = NETWORK.valueOf(name);
            if (network != null)
                return knowTypes.computeIfAbsent(name, (k) -> network);
        } catch (IllegalArgumentException ignore) {
        }
        try {
            final LitDataType profile = PROFILE.valueOf(name);
            if (profile != null)
                return knowTypes.computeIfAbsent(name, (k) -> profile);
        } catch (IllegalArgumentException ignore) {
        }

        return knowTypes.computeIfAbsent(name, (k) -> new GENERIC(k));
    }

    public class GENERIC implements LitDataType {
        private String name;

        public GENERIC(String name) {
            this.name = name;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            GENERIC other = (GENERIC) obj;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            return true;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}

package h2beaver.sobek.lit;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class FileLoader implements Common {
    protected Path dir;
    protected File[] files;
    protected Charset charset = Charset.forName("BIG5");

    public FileLoader(Path dir) {
        this.dir = dir;
        this.files = dir.toFile().listFiles();
    }

    public Path getDir() {
        return dir.toAbsolutePath();
    }

    private Optional<File> findFile(String name) {
        return Stream.of(files).filter(f -> f.getName().equalsIgnoreCase(name)).findFirst();
    }

    public File getFile(String name) {
        return findFile(name).orElse(null);
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public Charset getCharset(Charset charset) {
        return this.charset;
    }

    private static Predicate<String> POSSIBLE_HEADER = Pattern.compile("^[A-Za-z_]+[0-9.]+$").asPredicate();

    public List<LitRecord> readFile(String name) throws IOException {
        Optional<File> fileToLoad = findFile(name);
        if (fileToLoad.isPresent()) {
            String content = FileUtils.readFileToString(fileToLoad.get(), this.charset);
            int p = content.indexOf("\n");
            if (p > 0) {
                String firstLine = content.substring(0, p).trim();
                if (POSSIBLE_HEADER.test(firstLine)) {
                    LOGGER.info("Reading " + name + ", header line: " + firstLine);
                    return LitRecord.parse(name, content.substring(p));
                }
            }
            LOGGER.info("Reading " + name + ", no header line");
            return LitRecord.parse(name, content);
        }
        return Collections.emptyList();
    }

    public List<String[]> readCSV(String name) throws IOException, CsvException {
        Optional<File> fileToLoad = findFile(name);
        if (fileToLoad.isPresent()) {
            try (FileReader reader = new FileReader(fileToLoad.get(), this.charset)) {
                LOGGER.info("Reading csv format: " + name);
                CSVReader csvReader = new CSVReader(reader);
                List<String[]> list = new ArrayList<>();
                list.addAll(csvReader.readAll());
                reader.close();
                csvReader.close();
                return list;
            }
        }
        return Collections.emptyList();
    }
}

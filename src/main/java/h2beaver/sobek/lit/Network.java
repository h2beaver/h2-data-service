package h2beaver.sobek.lit;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.opencsv.exceptions.CsvBeanIntrospectionException;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.opencsv.exceptions.CsvValidationException;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineSegment;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;

import h2beaver.fondus.CRS;
import tw.fondus.commons.json.geojson.model.feature.Feature;
import tw.fondus.commons.json.geojson.model.feature.FeatureCollection;
import tw.fondus.commons.json.util.gson.GsonMapperRuntime;
import tw.fondus.commons.spatial.util.geojson.GeoJsonMapper;

public class Network implements Common {

    public enum SOBEK_NODE_TYPE {
        SBK_CONNECTIONNODE, SBK_PROFILE, UNDETERMINED;

        public static SOBEK_NODE_TYPE fromString(String value) {
            return Stream.of(SOBEK_NODE_TYPE.values()).filter(v -> v.name().equals(value)).findFirst()
                    .orElse(UNDETERMINED);
        }
    }

    public static final String NETWORK_NTW = "network.ntw";
    public static final List<String> FILES = Arrays.asList("nodes.dat", "network.cp", "network.tp");

    /* SOBEK layers        
        1: Topography (TP)
        2: Cross section (CR)
        3: Structure (ST)
        4: Friction (FR)
        5: Condition (CN)
        6: Initial conditions (IN)
        7: Meteo (MT)
        8: Dispersion (salt) (D)
        9: Grid (GR)
        10: Run time data (RT)
        11: Transport (TR)
        12: Substance (SB)
        13: Measurements (ME)
    
        [Topography layer]
        ntw=network.ntw
        net=network.tp
        cpt=network.cp
        dat=nodes.dat
        xyc=network.d12
        
        [Cross Section layer]
        net=network.cr
        def=profile.def
        dat=profile.dat
        
        [Structure layer]
        net=network.st
        def=struct.def
        dat=struct.dat
        con=control.def
        trg=trigger.def
        cmp=struct.cmp
        vlv=valve.tab
        dbs=struct.def
        
        [Grid layer]
        net=network.gr
        
        [Condition layer]
        net=network.cn
        flb=boundary.dat
        fll=lateral.dat
        sab=boundary.sal
        sal=lateral.sal
        fbd=boundlat.dat
    */

    public static class Node extends LitRecord {
        private Number x;
        private Number y;
        private SOBEK_NODE_TYPE node_type;

        protected Node(String source, String id) {
            super(source, LitDataType.NETWORK.NODE, id);
        }

        protected Node(String source, LitRecord record) {
            super(source, LitDataType.NETWORK.NODE, record.getId());
            this.attributes.putAll(record.attributes);
            this.children.putAll(record.children);
            this.values.addAll(record.values);

            this.valueUpdated();
        }

        @Override
        protected void valueUpdated() {
            if (this.attributes.containsKey("px")) {
                Value v = this.attributes.get("px");
                if (v.isNumber())
                    x = v.getNumberValue();
            }
            if (this.attributes.containsKey("py")) {
                Value v = this.attributes.get("py");
                if (v.isNumber())
                    y = v.getNumberValue();
            }
            if (this.attributes.containsKey("type")) {
                this.node_type = SOBEK_NODE_TYPE.fromString(this.getStringAttribute("type"));
            }
        }

        public SOBEK_NODE_TYPE nodeType() {
            return this.node_type;
        }

        public Reach reach() {
            Map<String, LitRecord> reaches = this.links(LitDataType.NETWORK.REACH);
            return reaches.values().stream().map(r -> (Reach) r).findAny().orElse(null);
        }

        public boolean isValid() {
            if (getId() == null)
                return false;
            String type = this.getStringAttribute("type");
            return type != null && type.length() > 0;
        }

        public boolean hasCoordinate() {
            return x != null && y != null;
        }

        public Coordinate getCoordinate() {
            if (hasCoordinate()) {
                return new Coordinate(SCALE_10000.makePrecise(x.doubleValue()),
                        SCALE_10000.makePrecise(y.doubleValue()));
            }
            return null;
        }

        public Point toPoint() {
            return hasCoordinate() ? TWD97.createPoint(this.getCoordinate()) : null;
        }
    }

    public static class Branch extends LitRecord {
        protected Node begin;
        protected Node end;
        protected LineSegment lineSegment;

        protected Branch(String source, String id) {
            super(source, LitDataType.NETWORK.BRCH, id);
        }

        public static Branch fromNetworkNTW(String[] s) {
            try {
                NetworkNTW.Branch n = NetworkNTW.buildBranch(s);
                return fromNetworkNTW(n);
            } catch (CsvBeanIntrospectionException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException
                    | CsvConstraintViolationException | CsvValidationException e) {
                throw new RuntimeException("Fail to parse branch from record " + s[0], e);
            }
        }

        public static Branch fromNetworkNTW(NetworkNTW.Branch n) {
            Branch b = new Branch(NETWORK_NTW, n.BrID);
            b.putAttribute("name", n.BrName);
            try {
                b.putAttribute("NETWORK_NTW_RAW_DATA", NetworkNTW.branchLine(n));
            } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            }
            b.putAttribute("type", n.BrObjID);
            int reach = n.BrReach;
            b.putAttribute("reach_number", reach);

            Node n1 = new Node(NETWORK_NTW, n.NdFrmID);
            n1.putAttribute("type", n.NdFrmObjID);
            n1.putAttribute("name", n.NdFrmName);
            n1.x = n.NdFrmX;
            n1.y = n.NdFrmY;
            b.begin = n1;

            Node n2 = new Node(NETWORK_NTW, n.NdToID);
            n2.putAttribute("type", n.NdToObjID);
            n2.putAttribute("name", n.NdToName);
            n2.x = n.NdToX;
            n2.y = n.NdToY;
            b.end = n2;

            n1.linkTo(b);
            n2.linkTo(b);
            return b;
        }

        protected Branch(String source, LitRecord record) {
            super(source, LitDataType.NETWORK.BRCH, record.getId());
            this.attributes.putAll(record.attributes);
            this.children.putAll(record.children);
            this.values.addAll(record.values);
        }

        public Reach reach() {
            Map<String, LitRecord> reaches = this.links(LitDataType.NETWORK.REACH);
            return reaches.values().stream().map(r -> (Reach) r).findAny().orElse(null);
        }

        public LineSegment toLineSegment() {
            if (this.lineSegment == null) {
                this.lineSegment = new LineSegment(begin.getCoordinate(), end.getCoordinate());
            }
            return this.lineSegment;
        }

        public LineString toLineString() {
            return toLineSegment().toGeometry(TWD97);
        }
    }

    public static class Reach extends LitRecord {
        protected final List<Node> nodes;
        protected final List<Branch> branches;

        /* NETWORK.NTW, Reach part, reference:            
            https://github.com/SNL-WaterPower/SNL-Delft3D-CEC/blob/master/src/tools_lgpl/matlab/quickplot/progsrc/private/sobek.m
            'S' 'ReachID'
            'S' 'ReachName'
            'S' 'NodeFromID'
            'S' 'NodeToID'
            'I' 'I1'
            'I' 'I2'
            'F' 'NodeFromX'
            'F' 'NodeFromY'
            'F' 'NodeToX'
            'F' 'NodeToY'
            'F' 'ReachLength'
            'I' 'VectorSplit' % 0=Full Vector, 1=By Vector, 2=By Coord
            'I' 'VectorSplitLen'
            'I' 'Equidistance'}; % -1=yes, 0=no
        */
        public static Reach fromNetworkNTW(String[] s) {
            return new Reach(NETWORK_NTW, s[0], s[2], s[3], new BigDecimal(s[10]));
        }

        protected Reach(String source, String id, String begin, String end, Number length) {
            super(source, LitDataType.NETWORK.REACH, id);
            nodes = new ArrayList<>();
            branches = new ArrayList<>();
            this.putAttribute("bn", begin);
            this.putAttribute("en", end);
            this.putAttribute("length", length);
        }

        public LineString toLineString() {
            return TWD97.createLineString(nodes.stream().map(n -> n.getCoordinate()).toArray(Coordinate[]::new));
        }
    }

    private Map<String, Node> nodes = new LinkedHashMap<>();
    private Map<String, Branch> branches = new LinkedHashMap<>();
    private Map<String, Reach> reaches = new LinkedHashMap<>();

    public Node findNode(String key) {
        return nodes.get(key);
    }

    public Branch findBranch(String key) {
        return branches.get(key);
    }

    public Reach findReach(String key) {
        return reaches.get(key);
    }

    public Node findNearestNode(Geometry g, double threshold) {
        ToDoubleFunction<Node> distance = n -> n.toPoint().distance(g);
        return nodes.values().stream()
                .filter(n -> (n != null && n.hasCoordinate() && distance.applyAsDouble(n) < threshold))
                .sorted(Comparator.comparingDouble(distance)).findFirst().orElse(null);
    }

    public Branch findNearestBranch(Geometry g, double threshold) {
        ToDoubleFunction<Branch> distance = b -> b.toLineString().distance(g);
        return branches.values().stream().filter(b -> (distance.applyAsDouble(b) < threshold))
                .sorted(Comparator.comparingDouble(distance)).findFirst().orElse(null);
    }

    public Reach findNearestReach(Geometry g, double threshold) {
        ToDoubleFunction<Reach> distance = r -> r.toLineString().distance(g);
        return reaches.values().stream().filter(r -> (distance.applyAsDouble(r) < threshold))
                .sorted(Comparator.comparingDouble(distance)).findFirst().orElse(null);
    }

    protected void compute() {
        reaches.values().stream().filter(r -> r.nodes.size() == 0).forEach(r -> {
            Node n0 = nodes.get(r.getStringAttribute("bn"));
            Node nn = nodes.get(r.getStringAttribute("en"));
            if (n0 != null && nn != null) {
                int i = 0;
                for (Node n = n0; !n.equals(nn); i++) {
                    r.nodes.add(n);
                    // Make reaches connected
                    n.links(LitDataType.NETWORK.REACH).values().stream().forEach(l -> r.linkTo(l, true));
                    n.linkTo(r);
                    final Node n1 = n;
                    Optional<Branch> o = r.branches.stream().filter(b -> b.begin != null && b.begin.equals(n1))
                            .findAny();
                    // If branches are not all connected, give up
                    if (!o.isPresent() || o.get().end.equals(n))
                        break;
                    Branch b = o.get();
                    n = b.end;
                    b.putAttribute("reach_order", i);
                }
                r.nodes.add(nn);
                nn.links(LitDataType.NETWORK.REACH).values().stream().forEach(l -> r.linkTo(l, true));
                nn.linkTo(r);
                r.branches.sort(Comparator.comparingLong(b -> Optional.ofNullable(b.getNumberAttribute("reach_order"))
                        .orElse(Long.MAX_VALUE).longValue()));
            }
        });
    }

    public String getStreamNetworkGeojson() {
        List<Feature> features = this.reaches.values().stream().map(r -> {
            Map<String, Object> properties = new HashMap<>();
            r.attributes.forEach((k, v) -> properties.put(k, v.getStringValue()));
            return new Feature(properties, GeoJsonMapper.fromJTS(r.toLineString()), r.getId());
        }).collect(Collectors.toList());

        FeatureCollection collection = FeatureCollection.builder().features(features)
                .crs(CRS.fromEPSG_SRID(TWD97.getSRID())).build();
        return GsonMapperRuntime.GEOJSON.toString(collection);
    }

    public String getAllNodesGeojson() {
        List<Feature> features = this.nodes.values().stream().filter(n -> n.isValid() && n.hasCoordinate()).map(n -> {
            Map<String, Object> properties = new HashMap<>();
            n.attributes.forEach((k, v) -> properties.put(k, v.getStringValue()));
            Reach r = n.reach();
            if (r != null) {
                properties.put("reach", r.getId());
            }
            return new Feature(properties, GeoJsonMapper.fromJTS(n.toPoint()), n.getId());
        }).collect(Collectors.toList());

        FeatureCollection collection = FeatureCollection.builder().features(features)
                .crs(CRS.fromEPSG_SRID(TWD97.getSRID())).build();
        return GsonMapperRuntime.GEOJSON.toString(collection);
    }

    private static Network loadNetworkNTW(FileLoader loader) throws IOException, CsvException {
        Network network = new Network();
        List<String[]> network_ntw_data = loader.readCSV(NETWORK_NTW);
        Map<String, List<String[]>> network_ntw_groups = new LinkedHashMap<>();
        String groupName = "[branches]";
        List<String[]> groupData = new LinkedList<>();
        for (Iterator<String[]> i = network_ntw_data.listIterator(1); i.hasNext();) {
            String[] line = i.next();
            if (line.length > 0) {
                if (line[0].startsWith("[")) {
                    network_ntw_groups.put(groupName, groupData);
                    groupName = line[0];
                    groupData = new LinkedList<>();
                } else
                    groupData.add(line);
            }
        }
        network_ntw_groups.put(groupName, groupData);

        List<String[]> reaches = network_ntw_groups.get("[Reach description]").stream().filter(s -> s.length == 14)
                .collect(Collectors.toList());
        reaches.stream().forEach(s -> {
            Reach r = Reach.fromNetworkNTW(s);
            network.reaches.put(r.getId(), r);
        });

        network_ntw_groups.get("[branches]").stream().filter(s -> s.length == 40).forEach(s -> {
            Branch b = Branch.fromNetworkNTW(s);
            Stream.of(b.begin, b.end).filter(Node::isValid).forEach(n -> network.nodes.put(n.getId(), n));
            int reach = b.getNumberAttribute("reach_number").intValue();
            if (reach > 0) {
                String reachId = reaches.get(reach - 1)[0];
                Reach r = network.reaches.get(reachId);
                b.putAttribute("reach", reachId);
                if (r != null) {
                    b.linkTo(r);
                    r.branches.add(b);
                }
            }
            network.branches.put(b.getId(), b);
        });

        network.compute();

        return network;
    }

    public static Network load(FileLoader loader) throws IOException, CsvException {
        Network network = loadNetworkNTW(loader);

        for (Iterator<String> i = FILES.iterator(); i.hasNext();) {
            String file = i.next();
            List<LitRecord> records = loader.readFile(file);
            records.stream().forEach(record -> {
                try {
                    LitDataType type = record.getType();
                    String id = record.getId();
                    if (id != null && type instanceof LitDataType.NETWORK) {
                        switch ((LitDataType.NETWORK) type) {
                            case NODE: {
                                Node merged = network.nodes.merge(id, new Node(file, record), (v1, v2) -> {
                                    v1.merge(v2);
                                    return v1;
                                });
                                LOGGER.info("Node merged into " + merged.getId());
                                break;
                            }
                            case BRCH: {
                                Branch merged = network.branches.merge(id, new Branch(file, record), (v1, v2) -> {
                                    v1.merge(v2);
                                    return v1;
                                });
                                LOGGER.info("Branch merged into " + merged.getId());
                                break;
                            }
                            default:
                        }
                    }
                } catch (Exception ex) {
                    LOGGER.error("Error when load NETWORK definitions", ex);
                }
            });
        }

        network.nodes.values().stream().forEach(n -> {
            if (n.attributes.containsKey("ci")) {
                String branchId = n.attributes.get("ci").getStringValue();
                Branch b = network.branches.get(branchId);
                if (b != null) {
                    n.linkTo(b, true);
                }
            }
        });

        network.branches.values().stream().forEach(b -> {
            if (b.attributes.containsKey("bn")) {
                String nodeId = b.attributes.get("bn").getStringValue();
                Node n = network.nodes.get(nodeId);
                if (n != null) {
                    b.begin = n;
                    n.linkTo(b);
                }
            }
            if (b.attributes.containsKey("en")) {
                String nodeId = b.attributes.get("en").getStringValue();
                Node n = network.nodes.get(nodeId);
                if (n != null) {
                    b.end = n;
                    n.linkTo(b);
                }
            }
        });

        return network;
    }
}

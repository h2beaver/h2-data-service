package h2beaver.sobek.lit;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Map;

public class LitRecordWriter {
    private static DecimalFormat df3 = new DecimalFormat("#.###");
    private static final char SPACE = ' ';
    private static final char NEWLINE = '\n';

    private static String asLITValue(Object v) {
        if (v == null)
            return "";
        if (v instanceof CharSequence) {
            return " '" + v + "'";
        }
        if (v instanceof Integer) {
            return " " + v.toString();
        }
        if (v instanceof Number) {
            return " " + df3.format(((Number) v).doubleValue());
        }
        return v.toString();
    }

    private final StringBuffer record = new StringBuffer();
    private final String type;
    private final Map<String, Object> properties;
    private boolean closed = false;

    public static LitRecordWriter of(String type, Map<String, Object> properties) {
        return new LitRecordWriter(type, properties);
    }

    private LitRecordWriter(String type, Map<String, Object> properties) {
        this.type = type.toUpperCase(Locale.ENGLISH);
        this.properties = properties;
        record.append(this.type);
    }

    protected final void appendKey(String key) {
        appendOptionalSpace();
        record.append(key);
    }

    protected final void appendOptionalSpace() {
        char lastChar = record.charAt(record.length() - 1);
        if (lastChar != SPACE && lastChar != NEWLINE)
            record.append(SPACE);
    }

    public LitRecordWriter optionalValue(String key, String defaultValue) {
        Object value = properties.get(key);
        if (value != null) {
            return addValue(key, value);
        }
        if (defaultValue != null) {
            return addValue(key, defaultValue);
        }
        return this;
    }

    public LitRecordWriter optionalValue(String key) {
        return optionalValue(key, null);
    }

    public LitRecordWriter addPropertyValue(String key, String propertyKey) {
        return addValue(key, properties.get(propertyKey));
    }

    public LitRecordWriter addPropertyValue(String key) {
        return addValue(key, properties.get(key));
    }

    public LitRecordWriter addValue(String key, Object value) {
        appendKey(key);
        record.append(asLITValue(value));
        return this;
    }

    public LitRecordWriter addTable(Object[][] table) {
        record.append(NEWLINE);
        record.append("TBLE");
        record.append(NEWLINE);
        for (int i = 0; i < table.length; i++) {
            Object[] values = table[i];
            for (int j = 0; j < values.length; j++) {
                record.append(asLITValue(values[j]));
            }
            record.append(SPACE);
            record.append("<");
            record.append(NEWLINE);
        }
        record.append("tble");
        record.append(NEWLINE);
        return this;
    }

    public synchronized void close() {
        if (this.closed)
            return;
        this.closed = true;
        appendOptionalSpace();
        record.append(this.type.toLowerCase(Locale.ENGLISH));
        record.append(NEWLINE);
    }

    public synchronized String build() {
        if (!closed)
            close();
        return record.toString();
    }

}

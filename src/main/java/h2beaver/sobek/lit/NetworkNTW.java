package h2beaver.sobek.lit;

import java.io.IOException;
import java.math.BigDecimal;

import com.opencsv.ICSVParser;
import com.opencsv.RFC4180Parser;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.exceptions.CsvBeanIntrospectionException;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.opencsv.exceptions.CsvValidationException;

public class NetworkNTW {
    public static final String FORMAT_WITH_QUOTE = "\"%s\"";
    public static final ColumnPositionMappingStrategy<Branch> BRANCH_MAPPER = new ColumnPositionMappingStrategy<>();
    public static final ICSVParser PARSER = new RFC4180Parser();

    static {
        String[] dummyHeader = new String[40];
        for (int i = 0; i < 40; i++) {
            dummyHeader[i] = Integer.toString(i);
        }
        BRANCH_MAPPER.setType(Branch.class);
        BRANCH_MAPPER.setColumnMapping(dummyHeader);
    }

    public static Branch buildBranch(String s) throws CsvBeanIntrospectionException, CsvRequiredFieldEmptyException,
            CsvDataTypeMismatchException, CsvConstraintViolationException, CsvValidationException, IOException {
        return buildBranch(PARSER.parseLine(s));
    }

    public static Branch buildBranch(String[] s) throws CsvBeanIntrospectionException, CsvRequiredFieldEmptyException,
            CsvDataTypeMismatchException, CsvConstraintViolationException, CsvValidationException {
        return BRANCH_MAPPER.populateNewBean(s);
    }

    public static String[] branchFields(Branch b) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        return BRANCH_MAPPER.transmuteBean(b);
    }
    public static String branchLine(Branch b) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        return String.join(",", branchFields(b));
    }

    public static class Branch {
        /* NETWORK.NTW, Branch part, reference:            
        https://github.com/SNL-WaterPower/SNL-Delft3D-CEC/blob/master/src/tools_lgpl/matlab/quickplot/progsrc/private/sobek.m
        Versions / 6.3 6.4 6.6
        'S' 'BrID' 0 0 0
        'S' 'BrName' 1 1 1
        'I' 'BrReach' 2 2 2
        'I' 'BrType' 3 3 3
        'S' 'BrObjID' 4 4 4
        'S' 'BrUserObjID' x 5 5
        'F' 'BrFrmZ' 5 6 6
        'F' 'BrToZ' 6 7 7
        'F' 'BrDepth' 7 8 8
        'F' 'BrLength' 8 9 9
        'F' 'BrLengthMap' 9 10 10
        'F' 'BrLengthUser' 10 11 11
        'F' 'BrVolume' 11 12 12
        'F' 'BrWidth' 12 13 13
        % -------
        'S' 'NdFrmID' 13 14 14
        'S' 'NdFrmName' 14 15 15
        'S' 'NdFrmArea' 15 16 16
        'I' 'NdFrmReach' 16 17 17
        'I' 'NdFrmType' 17 18 18
        'S' 'NdFrmObjID' 18 19 19
        'S' 'NdFrmUserObjID' x 20 20
        'F' 'NdFrmX' 19 21 21
        'F' 'NdFrmY' 20 22 22
        'F' 'NdFrmZ' 21 23 23
        'F' 'NdFrmReachDist' 22 24 24
        'I' 'NdFrmSys' 23 25 x
        'S' 'NdFrmSysStr' x x 25
        'I' 'NdFrmIden' 24 26 26
        % -------
        'S' 'NdToID' 25 27 27
        'S' 'NdToName' 26 28 28
        'S' 'NdToArea' 27 29 29
        'I' 'NdToReach' 28 30 30
        'I' 'NdToType' 29 31 31
        'S' 'NdToObjID' 30 32 32
        'S' 'NdToUserObjID' x 33 33
        'F' 'NdToX' 31 34 34
        'F' 'NdToY' 32 35 35
        'F' 'NdToZ' 33 36 36
        'F' 'NdToReachDist' 34 37 37
        'I' 'NdToSys' 35 38 x
        'S' 'NdToSysStr' x x 38
        'I' 'NdToIden' 36 39 39};
        */
        @CsvBindByPosition(position = 0, format = FORMAT_WITH_QUOTE)
        public String BrID;
        @CsvBindByPosition(position = 1, format = FORMAT_WITH_QUOTE)
        public String BrName;
        @CsvBindByPosition(position = 2)
        public int BrReach;
        @CsvBindByPosition(position = 3)
        public int BrType;
        @CsvBindByPosition(position = 4, format = FORMAT_WITH_QUOTE)
        public String BrObjID;
        @CsvBindByPosition(position = 5, format = FORMAT_WITH_QUOTE)
        public String BrUserObjID;
        @CsvBindByPosition(position = 6)
        public double BrFrmZ;
        @CsvBindByPosition(position = 7)
        public double BrToZ;
        @CsvBindByPosition(position = 8)
        public double BrDepth;
        @CsvBindByPosition(position = 9)
        public double BrLength;
        @CsvBindByPosition(position = 10)
        public double BrLengthMap;
        @CsvBindByPosition(position = 11)
        public double BrLengthUser;
        @CsvBindByPosition(position = 12)
        public double BrVolume;
        @CsvBindByPosition(position = 13)
        public double BrWidth;
        @CsvBindByPosition(position = 14, format = FORMAT_WITH_QUOTE)
        public String NdFrmID;
        @CsvBindByPosition(position = 15, format = FORMAT_WITH_QUOTE)
        public String NdFrmName;
        @CsvBindByPosition(position = 16, format = FORMAT_WITH_QUOTE)
        public String NdFrmArea;
        @CsvBindByPosition(position = 17)
        public int NdFrmReach;
        @CsvBindByPosition(position = 18)
        public int NdFrmType;
        @CsvBindByPosition(position = 19, format = FORMAT_WITH_QUOTE)
        public String NdFrmObjID;
        @CsvBindByPosition(position = 20, format = FORMAT_WITH_QUOTE)
        public String NdFrmUserObjID;
        @CsvBindByPosition(position = 21)
        public BigDecimal NdFrmX;
        @CsvBindByPosition(position = 22)
        public BigDecimal NdFrmY;
        @CsvBindByPosition(position = 23)
        public BigDecimal NdFrmZ;
        @CsvBindByPosition(position = 24)
        public double NdFrmReachDist;
        @CsvBindByPosition(position = 25, format = FORMAT_WITH_QUOTE)
        public String NdFrmSysStr;
        @CsvBindByPosition(position = 26)
        public int NdFrmIden;
        @CsvBindByPosition(position = 27, format = FORMAT_WITH_QUOTE)
        public String NdToID;
        @CsvBindByPosition(position = 28, format = FORMAT_WITH_QUOTE)
        public String NdToName;
        @CsvBindByPosition(position = 29, format = FORMAT_WITH_QUOTE)
        public String NdToArea;
        @CsvBindByPosition(position = 30)
        public int NdToReach;
        @CsvBindByPosition(position = 31)
        public int NdToType;
        @CsvBindByPosition(position = 32, format = FORMAT_WITH_QUOTE)
        public String NdToObjID;
        @CsvBindByPosition(position = 33, format = FORMAT_WITH_QUOTE)
        public String NdToUserObjID;
        @CsvBindByPosition(position = 34)
        public BigDecimal NdToX;
        @CsvBindByPosition(position = 35)
        public BigDecimal NdToY;
        @CsvBindByPosition(position = 36)
        public BigDecimal NdToZ;
        @CsvBindByPosition(position = 37)
        public double NdToReachDist;
        @CsvBindByPosition(position = 38)
        public String NdToSysStr;
        @CsvBindByPosition(position = 39, format = FORMAT_WITH_QUOTE)
        public int NdToIden;
    }
}

package h2beaver.sobek.lit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LitRecord {

    public static class Value {
        private Number n;
        private String s;

        private Value() {
        }

        private Value(String s) {
            if (s == null) return;
            if (s.equals("<")) this.s = s;
            else {
                try {
                    this.n = new BigDecimal(s);
                } catch (NumberFormatException e) {
                    // Accept string with correct quotation only
                    if (s.startsWith("'") && s.endsWith("'")) {
                        this.s = s.substring(1, s.length() - 1).replace(SPACE, " ");
                    }
                }
            }
        }

        public boolean hasValue() {
            return n != null || s != null;
        }

        public boolean isFlag() {
            return n == null && s == null;
        }

        public boolean isNumber() {
            return n != null;
        }

        public Number getNumberValue() {
            return n;
        }

        public String getStringValue() {
            if (n != null) return n.toString();
            return s;
        }

        public Object getValue() {
            if (this.isFlag()) return Boolean.TRUE;
            if (this.isNumber()) return this.getNumberValue();
            return this.getStringValue();
        }

        public String toString() {
            return getStringValue();
        }
    }

    private static final String SPACE = "\u2000";

    protected final String source;
    protected final LitDataType type;
    protected final Map<String, Value> attributes;
    protected final Map<LitDataType, LitRecord> children;
    protected final Map<LitDataType, Map<String, LitRecord>> links;
    protected final List<Value> values;
    private String id;

    private LitRecord(String source, LitDataType type) {
        this.source = source;
        this.type = type;
        this.attributes = new LinkedHashMap<>();
        this.children = new LinkedHashMap<>();
        this.links = new HashMap<>();
        this.values = new ArrayList<>();
    }

    protected LitRecord(String source, LitDataType type, String id) {
        this(source, type);
        this.id = id;
    }

    public final String getId() {
        if (this.id != null) return this.id;
        Value v = attributes.get("id");
        if (v != null) this.id = v.getStringValue();
        return this.id;
    }

    public final LitDataType getType() {
        return this.type;
    }

    public final void putAttribute(String key, Number n) {
        Value v = new Value();
        v.n = n;
        this.attributes.put(key, v);
        this.valueUpdated();
    }

    public final void putAttribute(String key, String s) {
        Value v = new Value();
        v.s = s;
        this.attributes.put(key, v);
        this.valueUpdated();
    }

    public final void linkTo(LitRecord another, boolean bothSide) {
        this.linkTo(another);
        if (bothSide) {
            another.linkTo(this);
        }
    }

    public final void linkTo(LitRecord another) {
        LitDataType type = another.type;
        Map<String, LitRecord> link = links.computeIfAbsent(type, k -> new LinkedHashMap<>());
        link.put(another.getId(), another);
        postLinkTo();
    }

    public final void merge(LitRecord another) {
        if (this.type.equals(another.type)) {
            this.attributes.putAll(another.attributes);
            this.values.addAll(another.values);
            for (Iterator<Map.Entry<LitDataType, Map<String, LitRecord>>> i = another.links.entrySet().iterator(); i.hasNext(); ) {
                Map.Entry<LitDataType, Map<String, LitRecord>> e = i.next();
                links.merge(e.getKey(), e.getValue(), (v1, v2) -> {
                    v1.putAll(v2);
                    return v1;
                });
            }
            postMerge();
        }
    }

    protected void valueUpdated() {
    }

    protected void postLinkTo() {
        valueUpdated();
    }

    protected void postMerge() {
        valueUpdated();
    }


    public final boolean hasAttribute(String key) {
        return this.attributes.containsKey(key);
    }

    public final Value getAttributeValue(String key) {
        return this.attributes.get(key);
    }

    public final Object getAttribute(String key) {
        Value v = getAttributeValue(key);
        if (v == null) return null;
        return v.getValue();
    }

    public final String getStringAttribute(String key) {
        return Optional.ofNullable(this.attributes.get(key)).map(v -> v.getStringValue()).orElse(null);
    }

    public final Number getNumberAttribute(String key) {
        return Optional.ofNullable(this.attributes.get(key)).map(v -> v.getNumberValue()).orElse(null);
    }

    public final Map<String, LitRecord> links(LitDataType key) {
        return this.links.computeIfAbsent(key, k -> new LinkedHashMap<>());
    }

    public final LitRecord child(LitDataType key) {
        return this.children.get(key);
    }

    public boolean equals(LitRecord another) {
        if (another == null) return false;
        // Type, Source and Id are the same then it's same
        return toString().equals(another.toString());
    }

    public String toString() {
        return "(" + this.type + "@" + this.source + ") " + getId();
    }

    public final Number[][] numberValueAsArray() {
        List<Number[]> rows = new ArrayList<>();
        List<Number> cols = new ArrayList<>();
        values.stream().forEach(v -> {
            if ("<".equals(v.getStringValue())) {
                rows.add(cols.toArray(new Number[cols.size()]));
                cols.clear();
            } else if (v.isNumber()) {
                cols.add(v.getNumberValue());
            }
        });
        if (cols.size() > 0) rows.add(cols.toArray(new Number[cols.size()]));

        return rows.toArray(new Number[rows.size()][]);
    }


    private static LitRecord tryParse(String source, Matcher matcher) {
        String type = matcher.group();
        if (!type.toUpperCase().equals(type)) {
            throw new IllegalArgumentException("Input should start with a type name all uppercase: " + type);
        }

        LitRecord record = new LitRecord(source, LitDataType.getType(type));
        boolean closed = false;
        String previousKey = null;
        while (matcher.find()) {
            String key = matcher.group();
            Value v = new Value(key);
            if (key != null) {
                if (key.equals(type.toLowerCase())) {
                    // end tag found, return it

                    closed = true;
                    break;
                }
                if (previousKey != null) {
                    String k = previousKey;
                    previousKey = null;
                    record.attributes.put(k, v);
                    if (v.hasValue()) {
                        continue;
                    }
                }
                if (key.length() == 4 && Character.isUpperCase(key.charAt(0)) && key.toUpperCase().equals(key)) {
                    // A type record
                    record.children.put(LitDataType.getType(key), tryParse(source, matcher));
                } else if (v.hasValue()) {
                    record.values.add(v);
                } else {
                    previousKey = key;
                }
            }
        }
        if (!closed) {
            throw new IllegalArgumentException("Input should end with a type name all uppercase: " + type + " (id: " + record.getId() + ")");
        }

        if (previousKey != null) {
            record.attributes.put(previousKey, new Value());
        }
        return record;
    }

    private static String processQuotation(String input) {
        Matcher matcher = Pattern.compile("'[^']*'").matcher(input.trim());
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String value = matcher.group();
            matcher.appendReplacement(sb, value.replaceAll("\\s", SPACE));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public final static List<LitRecord> parse(String source, String input) {
        Matcher matcher = Pattern.compile("\\S+").matcher(processQuotation(input));

        if (!matcher.find()) {
            return null;
        }

        List<LitRecord> records = new ArrayList<>();
        String type = matcher.group();
        if (!type.toUpperCase().equals(type)) {
            throw new IllegalArgumentException("Input should start with a type name all uppercase");
        }

        do {
            records.add(tryParse(source, matcher));
        } while ((matcher.find()));

        return records;
    }
}

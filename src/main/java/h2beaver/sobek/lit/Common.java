package h2beaver.sobek.lit;

import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface Common {
    PrecisionModel SCALE_10000 = new PrecisionModel(10000);
    GeometryFactory TWD97 = new GeometryFactory(SCALE_10000, 3826);
    Logger LOGGER = LoggerFactory.getLogger("SOBEK-LIT");

    public static String getRepoName(String region) {
        return "channel_network-" + region + "-vector";
    }
}
